require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.PORT

app.use(express.json())
app.use('/', require('./routes/authRoute'))
app.use('/article', require('./routes/articleRoute'))

app.use((error, req, res, next) => {
    res.send(error)
})

app.listen(port, () => {
    console.log(`listening on http://localhost${port}`);
})