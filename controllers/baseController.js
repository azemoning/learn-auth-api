const { nanoid } = require('nanoid')

class BaseController {
    constructor(model) {
        this.model = model
    }

    get(query) {
        this.model.findAll({
            where: query
        })
    }

    add(data) {
        this.model.create({
            id: nanoid(),
            ...data
        })
    }

    edit(id, data) {
        this.model.update(data, {
            where: id
        })
    }

    remove(id) {
        this.model.destroy({
            where: { id }
        })
    }
}

module.exports = BaseController